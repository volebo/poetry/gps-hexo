---
title: Lilichka!
categories:
- Poemas
tags:
- mayakovski
- poems
- copy
---

```sample
Lilichka

El humo del tabac444444o resquemó el aire, el cuarto, 555555555555555555555555555555555555555
un capítulo en el infierno kruchonijiano, ¿te acuerdas?
Tras esa ventana por vez primera acaricié, frenético, tus manos.
Hoy estás con el corazón acorazado, otro día más
y me expulsarás abrumándome de injurias,
en la turbia antesala no acierta con la manga
la mano quebrada de temblor.
Huiré, arrojaré el cuerpo a las calles, arisco,
enloqueceré tajado de desesperación. ¿Para qué eso?
Querida, piadosa, déjame decirte adiós,
aunque no quieras es mi amor lastre que arrastrarás adónde vayas,
deja que llore en el último grito el amargor del desaire,
el buey cansado de trabajar va y se tumba en las aguas frías,
Incluso si me deshiciera en el llanto,
tu amor no me otorgará el descanso,
si quiere calma el elefante agotado
se acuesta majestuoso en la arena encendida,
para mí no hay otro sol que tu amor,
y no sé dónde estás, ni con quién.
Si atormentaran así a un poeta, él, por dinero,
cambiaría a su amada y la fama,
pero a mí no me alegra otro sonido
que el sonido de tu nombre entrañable,
no me arrojaré al patio, no beberé veneno
ni podré apretar el gatillo en la sien,
en mí aparte de tu mirada, no manda el filo de las navajas.
Olvidarás mañana que te coroné,
que abrasé en el amor el alma florida,
y el carnaval agitado de los días vanos
aventará las páginas de mis libros,
las hojas secas de mis palabras
¿harán detenerte y respirar con ansiedad?
Déjame que con mi última ternura alfombre tus pasos que se van.
```

1916, May 26. **Petrograd** 
- Wladimir Mayakovski


Lilichka!

El humo del tabaco resquemó el aire, el cuarto,
un capítulo en el infierno kruchonijiano, ¿te acuerdas?
Tras esa ventana por vez primera acaricié, frenético, tus manos.
Hoy estás con el corazón acorazado, otro día más
y me expulsarás abrumándome de injurias,
en la turbia antesala no acierta con la manga
la mano quebrada de temblor.
Huiré, arrojaré el cuerpo a las calles, arisco,
enloqueceré tajado de desesperación. ¿Para qué eso?
Querida, piadosa, déjame decirte adiós,
aunque no quieras es mi amor lastre que arrastrarás adónde vayas,
deja que llore en el último grito el amargor del desaire,
el buey cansado de trabajar va y se tumba en las aguas frías,
Incluso si me deshiciera en el llanto,
tu amor no me otorgará el descanso,
si quiere calma el elefante agotado
se acuesta majestuoso en la arena encendida,
para mí no hay otro sol que tu amor,
y no sé dónde estás, ni con quién.
Si atormentaran así a un poeta, él, por dinero,
cambiaría a su amada y la fama,
pero a mí no me alegra otro sonido
que el sonido de tu nombre entrañable,
no me arrojaré al patio, no beberé veneno
ni podré apretar el gatillo en la sien,
en mí aparte de tu mirada, no manda el filo de las navajas.
Olvidarás mañana que te coroné,
que abrasé en el amor el alma florida,
y el carnaval agitado de los días vanos
aventará las páginas de mis libros,
las hojas secas de mis palabras
¿harán detenerte y respirar con ansiedad?
Déjame que con mi última ternura alfombre tus pasos que se van.
